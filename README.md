![OS](https://badgen.net/badge/OS/Android?icon=https://raw.githubusercontent.com/androiddevnotes/awesome-android-kotlin-apps/master/assets/android.svg&color=3ddc84)
![Kotlin](https://img.shields.io/badge/Kotlin-1.4.31-blue)
![Build Android](https://gitlab.com/VMagnan/apprecette/badges/master/pipeline.svg)

# App Recette

<p align="center">
    <img src="app/src/main/ic_launcher-playstore.png" alt="App Recette logo" width="80" height="80">
</p>

A Recipes Application in _Android_with_Kotlin_.

# DEMO

<img src="/uploads/e920e936f9a25ad5478e559749e0030a/Screenshot_1618393123.png" width="200" height="400">
<img src="/uploads/77f8d2ba1d4598264da3c0c505c66805/Screenshot_1618393159.png" width="200" height="400">
<img src="/uploads/fe1574f6ff06065db70f073d41722503/Screenshot_1618393136.png" width="200" height="400">
<img src="/uploads/ce5d9c72996a3f2fd21e08b66e040113/Screenshot_1618393146.png" width="200" height="400">
<img src="/uploads/60836261994dbb032bfd6b46db20a882/Screenshot_1618393150.png" width="200" height="400">
<img src="/uploads/a8073cec430fe633ee6dcbcf5e999b6d/Screenshot_1618393154.png" width="200" height="400">

This is a showcase of our Application.<br/>
This project implements **Recipes list**, **Calendar with Recall items**, **User profile** and **Search using filters**.<br/>
Using _Android SDK_ and extension function with _Kotlin_.<br/>
It was create from 'Bottom Navigation Activity' template from _Android Studio_.

<!-- TABLE OF CONTENTS -->
<details close>
    <summary>Table of Contents</summary>
    <ul>
        <li>
            <a href="#getting-started">Getting Started</a>
            <ul>
                <li><a href="#prerequisites">Prerequisites</a></li>
                <li><a href="#installation">Installation</a></li>
            </ul>
        </li>
        <li><a href="#dependencies">Dependencies</a></li>
        <li><a href="#usage">Usage</a></li>
        <li><a href="#roadmap">Roadmap</a></li>
        <li><a href="#contributing">Contributing</a></li>
        <li><a href="#license">License</a></li>
        <li><a href="#members">Members</a></li>
    </ul>
</details>

## Getting Started

This is an _Android_ project made with _Kotlin_ language.

### Prerequisites

Install [Android Studio](https://developer.android.com/studio), see instructions
on [ChillCoding.com](https://www.chillcoding.com/blog/2016/08/03/android-studio-installation/),
in FR.

### Installation

1. Clone the repo
    '''sh
    git clone https://gitlab.com/VMagnan/apprecette.git
    '''
2. Choose, _Open an Existing Project_ in _Android Studio_

## Dependencies

### Libraries

- Retrofit2 -> https://square.github.io/retrofit/
- Picasso -> https://square.github.io/picasso/
- Room -> https://developer.android.com/jetpack/androidx/releases/room?authuser=1

### Other
* [View Binding, Part of Android Jetpack](https://developer.android.com/topic/libraries/view-binding)

## Usage

Use this space to show beautiful User Interface elements.

## Roadmap

See the [open issues](https://gitlab.com/VMagnan/apprecette/-/issues) for a list of proposed features (and known issues).

## Contributing

 Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

 1. Fork the Project
 2. Create your Feature Branch ('git checkout -b feature/AmazingFeature')
 3. Commit your Changes ('git commit -m 'Add some AmazingFeature'')
 4. Push to the Branch ('git push origin feature/AmazingFeature')
 5. Open a pull Request

## License

Distributed under the GNU General Public License v3.0. See [LICENSE](https://github.com/machadaCosta/basic-ui/blob/main/LICENSE) for more information (cf. [Choose an open source license](https://choosealicense.com/)).

## Members

ANDRE Dylan, on [LinkedIn](https://www.linkedin.com/in/dylan-andre-bbb48714b/).<br/>
MAGNAN Vicente, on [LinkedIn](https://www.linkedin.com/in/vicente-magnan-39999a182/).
