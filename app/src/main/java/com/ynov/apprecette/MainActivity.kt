package com.ynov.apprecette

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.ynov.apprecette.data.RecipesList
import com.ynov.apprecette.data.RecipesService
import com.ynov.apprecette.database.recipe.RecipeDatabase
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    //Variables d'accès à la database des recettes et des preferences utilisateurs
    private lateinit var db: RecipeDatabase
    private lateinit var sharedPreferences: SharedPreferences

    //Variable view binding
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Creation du view binding
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //Creation de la vue de la navigation
        val navView: BottomNavigationView = binding.navView

        //Creation du controlleur de la navigation
        val navController = findNavController(R.id.nav_host_fragment)

        // Ajout de chaque fragment dans le navigateur
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_search, R.id.navigation_calendar, R.id.navigation_profile))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //Définition des variables
        db = RecipeDatabase.getRecipeDatabase(applicationContext)
        sharedPreferences = getSharedPreferences("USER_PREFERENCES", Context.MODE_PRIVATE)

        //Appel de l'API
        getAPIData()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //Retourne à l'écran précédent
        findNavController(R.id.nav_host_fragment).popBackStack()
        return super.onOptionsItemSelected(item)
    }

    /**
     * Récupération des données de l'API
     * */
    private fun getAPIData() {
        //Création du service retrofit
        val retrofit = Retrofit.Builder().baseUrl("https://api.spoonacular.com/recipes/").addConverterFactory(GsonConverterFactory.create()).build()
        val service = retrofit.create(RecipesService::class.java)

        //Récupère les préférences de l'utilisateur
        val favDish = sharedPreferences.getString("favDish", null)?.toLowerCase(Locale.getDefault())
        val favFood = sharedPreferences.getString("favFood", null)?.toLowerCase(Locale.getDefault())

        //Thread lancé pour réinitialiser la databse accueillant les recettes aléatoires
        Thread {
            db.clearAllTables()
        }.start()

        //Création de l'appel commun à l'API random
        val randomCall = service.getRandomListOfRecipes("")

        //Appel à la requete
        randomCall.enqueue(object : Callback<RecipesList> {
            override fun onResponse(call: Call<RecipesList>, response: Response<RecipesList>) {
                apiResponse(response, "default")
            }

            override fun onFailure(call: Call<RecipesList>, t: Throwable) {
                error("ERROR GET RANDOM RECIPES")
            }
        })

        //Si la préférence n'est pas null on fait l'appel à la requete
        if(favDish != null) {
            val randomDishCall = service.getRandomListOfRecipes(favDish)
            randomDishCall.enqueue(object : Callback<RecipesList> {
                override fun onResponse(call: Call<RecipesList>, response: Response<RecipesList>) {
                    apiResponse(response, "dish")
                }

                override fun onFailure(call: Call<RecipesList>, t: Throwable) {
                    error("ERROR GET RANDOM RECIPES")
                }
            })
        }

        //Si la préférence n'est pas null on fait l'appel à la requete
        if(favFood != null) {
            val randomDishCall = service.getRandomListOfRecipes(favFood)
            randomDishCall.enqueue(object : Callback<RecipesList> {
                override fun onResponse(call: Call<RecipesList>, response: Response<RecipesList>) {
                    apiResponse(response, "food")
                }

                override fun onFailure(call: Call<RecipesList>, t: Throwable) {
                    error("ERROR GET RANDOM RECIPES")
                }
            })
        }
    }

    /**
     * Fonction qui vérifie si l'item est présent dans la database des recettes favorites
     * @param id Id de l'item
     * */
    private fun isFavorite(id: Int) : Boolean {
        val db = RecipeDatabase.getFavoriteDatabase(applicationContext)
        return db.recipeDAO().isRowIsExist(id)
    }

    /**
     * Fonction qui gère la réponse à l'API
     * @param response Reponse de l'API
     * @param randomType Type de la recette recue
     * */
    private fun apiResponse(response: Response<RecipesList>, randomType: String) {

        //Récupère la liste dans le corps de la reponse
        val recipesList = response.body()

        if(response.code() == 200) {
            //Crée une liste temporaire de RecipeItem
            val items = ArrayList<RecipeItem>()

            //Lance un thread pour ne pas bloquer l'UI
            Thread {
                //Pour chaque recette dans la liste de recette
                for (recipe in recipesList.recipes) {
                    //On crée un item en liant les données et on l'ajoute à la liste temporaire
                    val item = RecipeItem(null, recipe.name, recipe.imageUrl, recipe.readyInMinutes, recipe.servings, recipe.likesNumber, recipe.instructions, recipe.healthScore, isFavorite(recipe.id), randomType)
                    items.add(item)
                }

                //On insère tous les items dans la database des recettes aléatoires
                db.recipeDAO().insertAll(items)
            }.start()
        }
    }
}