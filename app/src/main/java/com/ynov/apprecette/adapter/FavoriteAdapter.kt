package com.ynov.apprecette.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.ynov.apprecette.R
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.ItemFavoriteBinding

class FavoriteAdapter(val items: Array<RecipeItem>, val adapterOnClick: (RecipeItem) -> Unit) : RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemFavoriteBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Fonction de binding des données
         * @param recipe Item d'une recette
         * */
        fun bindFavorite(recipe: RecipeItem) {
            with(recipe) {
                //Bind les données
                binding.itemTxt.text = name
                val imgView = binding.itemImg
                Picasso.get().load(imageUrl).fit().into(imgView)

                if(imageUrl == null) {
                    imgView.setImageResource(R.drawable.ic_browser_not_supported_black)
                }

                //Bind l'action de suppression d'un item
                binding.removeButton.setOnClickListener {
                    adapterOnClick(this)
                }

                binding.root.setOnClickListener {
                    val navController = findNavController(itemView)
                    val bundle = Bundle()
                    bundle.putString("source", "favorite")
                    bundle.putInt("item_id", uid!!)

                    navController.navigate(R.id.action_navigation_profile_to_navigation_recipe, bundle)
                }
            }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFavoriteBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindFavorite(items[position])
    }
}