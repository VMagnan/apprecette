package com.ynov.apprecette.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.ynov.apprecette.R
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.ItemRecipeBinding

class RandomRecipeAdapter(var items: Array<RecipeItem>, val adapterOnClick: (RecipeItem, Boolean) -> Unit) : RecyclerView.Adapter<RandomRecipeAdapter.ViewHolder>(), Filterable {

    //Tableayu d'items filtrés
    var itemsFiltered: Array<RecipeItem> = arrayOf()

    inner class ViewHolder(val binding: ItemRecipeBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Fonction de binding des données
         * @param recipe Item d'une recette
         * */
        fun bindRecipe(recipe: RecipeItem) {
            with(recipe) {
                //Bind les données
                binding.itemName.text = name

                val imgView = binding.itemImg
                Picasso.get().load(imageUrl).fit().into(imgView)

                if(imageUrl == null) {
                    imgView.setImageResource(R.drawable.ic_browser_not_supported_black)
                }

                binding.itemServingsTxt.text = servings.toString()
                binding.itemLikesNumberTxt.text = likesNumber.toString()
                binding.itemReadyInTxt.text = readyInMinutes.toString()

                //Bind l'action d'appui sur un item qui navigue vers le fragment en passant l'id de l'item
                binding.root.setOnClickListener {
                    val navController = findNavController(itemView)
                    val bundle = Bundle()
                    bundle.putString("source", "random")
                    bundle.putInt("item_id", uid!!)
                    navController.navigate(R.id.action_navigation_home_to_navigation_recipe, bundle)
                }

                //Selon l'état favori affiche un icone rempli ou non
                if(isFavorite) {
                    binding.itemFavoriteImg.setImageResource(R.drawable.ic_star)
                } else {
                    binding.itemFavoriteImg.setImageResource(R.drawable.ic_star_outline)
                }

                //Gère l'appuie sur l'icone favori
                binding.itemFavoriteImg.setOnClickListener{
                    if(isFavorite) {
                        binding.itemFavoriteImg.setImageResource(R.drawable.ic_star_outline)
                        items[bindingAdapterPosition].isFavorite = false
                    } else {
                        binding.itemFavoriteImg.setImageResource(R.drawable.ic_star)
                        items[bindingAdapterPosition].isFavorite = true
                    }
                    adapterOnClick(this, isFavorite)
                    notifyItemChanged(bindingAdapterPosition)
                }
            }
        }
    }

    override fun getItemCount(): Int = itemsFiltered.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemRecipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindRecipe(itemsFiltered[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                //Crée des tableaux temporaires
                val tempList = ArrayList<RecipeItem>()
                val filterResults = FilterResults()

                //Si la contrainte de filtre est vide
                if(constraint.isEmpty()) {
                    //On charge tous les items
                    filterResults.values = items
                }
                //Sinon
                else {
                    //Pour toutes les recettees
                    for (recipe in items) {
                        //Si la contrainte de filtre est vraie
                        if (recipe.randomType == constraint) {
                            //On l'ajoute à la liste temporaire
                            tempList.add(recipe)
                        }
                    }
                    //On charge la liste et on la retourne
                    filterResults.values = tempList.toTypedArray()
                }

                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                //Efface les données présentes
                itemsFiltered.toMutableList().clear()

                //Récupère les nouvelles données filtrées et on notifie un changement
                itemsFiltered = results?.values as Array<RecipeItem>
                notifyDataSetChanged()
            }
        }
    }
}