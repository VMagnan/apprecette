package com.ynov.apprecette.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.ynov.apprecette.database.reminder.ReminderItem
import com.ynov.apprecette.databinding.ItemReminderBinding

class ReminderAdapter(var items: Array<ReminderItem>) : RecyclerView.Adapter<ReminderAdapter.ViewHolder>(), Filterable {

    var itemsFull: Array<ReminderItem> = items

    class ViewHolder(val binding: ItemReminderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindReminder(reminder : ReminderItem) {
            with(reminder) {
                binding.cardTitle.text = title
                binding.cardDate.text = moment
            }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemReminderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindReminder(items[position])
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val tempList = ArrayList<ReminderItem>()
                for (item in itemsFull) {
                    if(item.date == constraint.toString()) {
                        tempList.add(item)
                    }
                }
                val filterResults = FilterResults()
                filterResults.values = tempList.toTypedArray()
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                items.toMutableList().clear()
                items = results?.values as Array<ReminderItem>
                notifyDataSetChanged()
            }
        }
    }
}