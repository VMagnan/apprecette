package com.ynov.apprecette.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.ynov.apprecette.R
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.ItemRecipeBinding

class SearchRecipeAdapter(var items: Array<RecipeItem>, val adapterOnClick: (RecipeItem, Boolean) -> Unit) : RecyclerView.Adapter<SearchRecipeAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: ItemRecipeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindRecipe(recipe: RecipeItem) {
            with(recipe) {
                binding.itemName.text = name

                val imgView = binding.itemImg
                Picasso.get().load(imageUrl).fit().into(imgView)

                if(imageUrl == null) {
                    imgView.setImageResource(R.drawable.ic_browser_not_supported_black)
                }

                binding.itemServingsTxt.text = servings.toString()
                binding.itemLikesNumberTxt.text = likesNumber.toString()
                binding.itemReadyInTxt.text = readyInMinutes.toString()

                if(isFavorite) {
                    binding.itemFavoriteImg.setImageResource(R.drawable.ic_star)
                } else {
                    binding.itemFavoriteImg.setImageResource(R.drawable.ic_star_outline)
                }

                binding.itemFavoriteImg.setOnClickListener{
                    if(isFavorite) {
                        binding.itemFavoriteImg.setImageResource(R.drawable.ic_star_outline)
                        items[bindingAdapterPosition].isFavorite = false
                    } else {
                        binding.itemFavoriteImg.setImageResource(R.drawable.ic_star)
                        items[bindingAdapterPosition].isFavorite = true
                    }
                    adapterOnClick(this, isFavorite)
                    notifyItemChanged(bindingAdapterPosition)
                }
            }
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemRecipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindRecipe(items[position])
    }
}