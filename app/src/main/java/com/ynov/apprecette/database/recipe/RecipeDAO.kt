package com.ynov.apprecette.database.recipe;

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RecipeDAO {
    @Query("SELECT * FROM RecipeItem")
    fun getAll(): LiveData<List<RecipeItem>>

    @Query("SELECT * FROM RecipeItem WHERE uid=:id")
    fun getLiveDataById(id: Int): LiveData<RecipeItem>

    @Query("SELECT * FROM RecipeItem WHERE uid=:id")
    fun getById(id: Int): RecipeItem

    @Query("SELECT EXISTS(SELECT * FROM RecipeItem WHERE uid = :id)")
    fun isRowIsExist(id: Int) : Boolean

    @Insert
    fun insertAll(recipes: List<RecipeItem>)

    @Insert
    fun insertOne(recipe: RecipeItem)

    @Query("DELETE FROM RecipeItem")
    fun deleteAll()

    @Delete
    fun deleteOne(recipe: RecipeItem)

    @Update
    fun updateRecipe(recipe: RecipeItem)

}
