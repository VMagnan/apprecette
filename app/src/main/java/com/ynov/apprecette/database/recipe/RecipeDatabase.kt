package com.ynov.apprecette.database.recipe

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [RecipeItem::class], version = 1)
abstract class RecipeDatabase: RoomDatabase() {

    abstract fun recipeDAO(): RecipeDAO

    companion object {
        private var RECIPE_INSTANCE: RecipeDatabase? = null
        private var FAVORITE_INSTANCE: RecipeDatabase? = null

        internal fun getRecipeDatabase(context: Context): RecipeDatabase {
            if (RECIPE_INSTANCE == null) {
                synchronized(RecipeDatabase::class.java) {
                    if (RECIPE_INSTANCE == null) {
                        RECIPE_INSTANCE = Room.databaseBuilder(context.applicationContext, RecipeDatabase::class.java, "recipes_database").build()
                    }
                }
            }
            return RECIPE_INSTANCE!!
        }

        internal fun getFavoriteDatabase(context: Context): RecipeDatabase {
            if (FAVORITE_INSTANCE == null) {
                synchronized(RecipeDatabase::class.java) {
                    if (FAVORITE_INSTANCE == null) {
                        FAVORITE_INSTANCE = Room.databaseBuilder(context.applicationContext, RecipeDatabase::class.java, "favorites_database").build()
                    }
                }
            }
            return FAVORITE_INSTANCE!!
        }
    }
}