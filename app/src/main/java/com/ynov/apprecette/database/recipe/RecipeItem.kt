package com.ynov.apprecette.database.recipe

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RecipeItem(
    @PrimaryKey(autoGenerate = true) var uid: Int? = null,
    var name: String?,
    var imageUrl: String?,
    var readyInMinutes: Int?,
    var servings: Int?,
    var likesNumber: Int?,
    var instructions: String?,
    var healthScore: Int?,
    var isFavorite: Boolean,
    var randomType: String,
)

/*
@Entity
data class IngredientItem(
    @PrimaryKey var uid: Int = 0,
    var amount: Int?,
    var imageUrl: String?,
    var name: String?,
    var unit: String?,
    var original: String?,
)*/
