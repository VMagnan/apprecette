package com.ynov.apprecette.database.reminder;

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ReminderDAO {
    @Query("SELECT * FROM ReminderItem")
    fun getAll(): LiveData<List<ReminderItem>>

    @Insert
    fun insert(recipes: ReminderItem)

    @Query("DELETE FROM ReminderItem")
    fun deleteAll()
}