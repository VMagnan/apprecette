package com.ynov.apprecette.database.reminder

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [ReminderItem::class], version = 1)
abstract class ReminderDatabase: RoomDatabase() {

    abstract fun reminderDAO(): ReminderDAO

    companion object {
        private var INSTANCE: ReminderDatabase? = null

        internal fun getDatabase(context: Context): ReminderDatabase {
            if (INSTANCE == null) {
                synchronized(ReminderDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext, ReminderDatabase::class.java, "reminder_database").build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}