package com.ynov.apprecette.database.reminder

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ReminderItem(
    @PrimaryKey(autoGenerate = true) var uid: Int? = null,
    var date: String?,
    var title: String?,
    var moment: String?
)