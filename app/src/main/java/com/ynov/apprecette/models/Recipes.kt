package com.ynov.apprecette.data

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

data class RecipesList(
    @SerializedName("recipes", alternate = ["results"])
    val recipes: List<Recipes>
)

data class Recipes(
    @SerializedName("id")
    val id: Int,

    @SerializedName("title")
    val name: String,

    @SerializedName("image")
    val imageUrl: String,

    @SerializedName("readyInMinutes")
    val readyInMinutes: Int,

    @SerializedName("servings")
    val servings: Int,

    @SerializedName("aggregateLikes")
    val likesNumber: Int,

    @SerializedName("instructions")
    val instructions: String,

    @SerializedName("healthScore")
    val healthScore: Int,

//    @SerializedName("dishTypes")
//    val dishTypes: Array<String>,

/*    @SerializedName("extendedIngredients")
    val extendedIngredients: ArrayList<ExtendedIngredient>*/
)

data class ExtendedIngredient(

    @SerializedName("amount")
    val amount: Int,

    @SerializedName("image")
    val imageUrl: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("unit")
    val unit: String,

    @SerializedName("original")
    val original: String,
)

interface RecipesService {
    @GET("random")
    fun getRandomListOfRecipes( @Query("tags") tags: String, @Query("apiKey") key: String = "19034eb5fa2340ed804cf62cf769d775", @Query("number") to: Int = 20) : Call<RecipesList>

    @GET("complexSearch")
    fun searchRecipes(@Query("query") query: String, @Query("apiKey") key: String = "19034eb5fa2340ed804cf62cf769d775", @Query("number") to: Int = 100, @Query("addRecipeInformation") addDesc: Boolean = true) : Call<RecipesList>
}

