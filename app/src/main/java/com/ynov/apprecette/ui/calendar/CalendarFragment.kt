package com.ynov.apprecette.ui.calendar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ynov.apprecette.adapter.ReminderAdapter
import com.ynov.apprecette.database.reminder.ReminderDatabase
import com.ynov.apprecette.databinding.FragmentCalendarBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class CalendarFragment : Fragment() {
    private var _binding: FragmentCalendarBinding? = null
    private val binding get() = _binding!!

    companion object {
        lateinit var selectedDate : String
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentCalendarBinding.inflate(inflater, container, false)
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        binding.txtViewDateOfTheDay.text = formatter.format(binding.myCalendar.date)
        val cal = Calendar.getInstance()

        selectedDate = formatter.format(binding.myCalendar.date)

        binding.myCalendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            cal.set(year, month, dayOfMonth)

            val formattedDate = formatter.format(cal.time)
            binding.txtViewDateOfTheDay.text = formattedDate
            selectedDate = formattedDate
            filterData()
        }

        binding.fabAdd.setOnClickListener{
            val args = Bundle()
            args.putString("selectedDate", selectedDate)
            val dialog = ReminderFragment()
            dialog.arguments = args
            dialog.show(this.parentFragmentManager, "ReminderDialog")
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        GlobalScope.launch(Dispatchers.Default) {
            launch(Dispatchers.Main) {
                getData()
            }
        }
    }

    private fun getData() {
        val db = ReminderDatabase.getDatabase(requireActivity().applicationContext)
        binding.reminderRecyclerView.layoutManager = GridLayoutManager(activity, 1, GridLayoutManager.VERTICAL, false)

        db.reminderDAO().getAll().observe(viewLifecycleOwner) {
            binding.reminderRecyclerView.adapter = ReminderAdapter(it.toTypedArray())
            filterData()
        }
    }

    private fun filterData() {
        if(binding.reminderRecyclerView.adapter != null) {
            val adapter = binding.reminderRecyclerView.adapter as ReminderAdapter
            adapter.filter.filter(selectedDate)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}