 package com.ynov.apprecette.ui.calendar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.DialogFragment
import com.ynov.apprecette.R
import com.ynov.apprecette.database.recipe.RecipeDatabase
import com.ynov.apprecette.database.reminder.ReminderDatabase
import com.ynov.apprecette.database.reminder.ReminderItem
import com.ynov.apprecette.databinding.FragmentReminderBinding

 class ReminderFragment : DialogFragment() {
     private var _binding: FragmentReminderBinding? = null
     private val binding get() = _binding!!
     private lateinit var selectedDate : String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentReminderBinding.inflate(inflater, container, false)

        selectedDate = arguments?.getString("selectedDate")!!

        val items = listOf("Matin", "Midi", "Soir")
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)
        (binding.reminderDateLayout.editText as? AutoCompleteTextView)?.setAdapter(adapter)

        val db = RecipeDatabase.getFavoriteDatabase(requireActivity().applicationContext)
        db.recipeDAO().getAll().observe(this) {
            if(it.isEmpty()) {
                val favoriteItems = listOf("Aucune recette...")
                val favoriteAdapter = ArrayAdapter(requireContext(), R.layout.list_item, favoriteItems)
                (binding.reminderRecipes.editText as? AutoCompleteTextView)?.setAdapter(favoriteAdapter)
            } else {
                val recipeName: MutableList<String> = mutableListOf()
                for (recipeItem in it) {
                    recipeName.add(recipeItem.name!!)
                }
                val favoriteAdapter = ArrayAdapter(requireContext(), R.layout.list_item, recipeName)
                (binding.reminderRecipes.editText as? AutoCompleteTextView)?.setAdapter(favoriteAdapter)
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupClickListeners()
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun setupClickListeners() {
        binding.validButton.setOnClickListener {
            val reminderItem = ReminderItem(null, selectedDate, binding.reminderRecipesTxtView.text.toString(), binding.reminderDate.text.toString())
            val db = ReminderDatabase.getDatabase(requireActivity().applicationContext)
            Thread {
                db.reminderDAO().insert(reminderItem)
            }.start()
            dismiss()
        }

        binding.annulButton.setOnClickListener {
            dismiss()
        }
    }
}