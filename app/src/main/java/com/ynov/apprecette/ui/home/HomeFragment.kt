package com.ynov.apprecette.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ynov.apprecette.adapter.RandomRecipeAdapter
import com.ynov.apprecette.database.recipe.RecipeDatabase
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.FragmentHomeBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {

    //Variables view binding
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    //Variables d'accès aux databases et préférences utilisateurs
    private lateinit var favoriteDatabase: RecipeDatabase
    private lateinit var recipeDatabase: RecipeDatabase
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View {

        //Création du ViewBinding
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        //Création des accès aux databases et préférences utilisateurs
        favoriteDatabase = RecipeDatabase.getFavoriteDatabase(requireActivity().applicationContext)
        recipeDatabase = RecipeDatabase.getRecipeDatabase(requireActivity().applicationContext)
        sharedPreferences = activity?.getSharedPreferences("USER_PREFERENCES", Context.MODE_PRIVATE)!!

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Récupération des préférences de l'utilisateur
        val favDish = sharedPreferences.getString("favDish", null)
        val favFood = sharedPreferences.getString("favFood", null)

        //Lance un thread pour initialiser les données
        GlobalScope.launch(Dispatchers.Default) {
            launch(Dispatchers.Main) {
                setData()
            }
        }

        //Si la préférence n'a pas été configurée on désactive le Chip spécifique
        if(favDish == null) binding.chipDish.isEnabled = false

        //Si la préférence n'a pas été configurée on désactive le Chip spécifique
        if(favFood == null) binding.chipFood.isEnabled = false

        //Fait appel à une fonction pour gérer le click sur un Chip
        binding.chipGroup.setOnCheckedChangeListener { _, _ -> handleChipSelected() }
    }

    /**
     * Fonction qui va définir un observer sur les données en database pour les afficher dans la recyclerView
     * */
    private fun setData() {
        //Défini la configuration du recyclerview
        binding.recipeRecyclerView.layoutManager = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)

        //Affiche la progressBar
        binding.indeterminateBar.visibility = View.VISIBLE

        //Observe les données en database
        recipeDatabase.recipeDAO().getAll().observe(viewLifecycleOwner) {

            //Crée l'adapter en passant les données et une fonction lambda gérant le click sur l'ajout vers favoris
            val adapter = RandomRecipeAdapter(it.toTypedArray()) { item, isFavorite ->
                handleFavClick(
                    item,
                    isFavorite
                )
            }

            //Configure l'adapter
            binding.recipeRecyclerView.adapter = adapter

            //Initialise le filtrage des données
            handleChipSelected()

            //Enleve la progressBar
            binding.indeterminateBar.visibility = View.GONE
        }
    }

    /**
     * Filtre les données du recyclerView selon le type passé en paramètre
     * @param randomType Type du filtre des données
     * */
    private fun filterData(randomType: String) {
        //Affiche la progressBar
        binding.indeterminateBar.visibility = View.VISIBLE

        //Si l'adapter n'est pas null
        if (binding.recipeRecyclerView.adapter != null) {
            //On le récupère
            val adapter = binding.recipeRecyclerView.adapter as RandomRecipeAdapter

            //On appelle la fonction de filtre en passant en paramètre le type de filtre choisi
            adapter.filter.filter(randomType)
        }

        //Enleve la progressBar
        binding.indeterminateBar.visibility = View.GONE
    }

    /**
     * Fonction qui gère l'ajout d'une recette en base de données des favoris et met à jour celles des recette aléatoires
     * @param item Item de la recette choisi
     * @param isFav Etat favori de l'item
     * */
    private fun handleFavClick(item: RecipeItem, isFav: Boolean) {
        //Lance un thread pour éviter les conflits avec l'UI
        Thread {
            if (isFav) {
                favoriteDatabase.recipeDAO().insertOne(item)
                recipeDatabase.recipeDAO().updateRecipe(item)
            } else {
                favoriteDatabase.recipeDAO().deleteOne(item)
                recipeDatabase.recipeDAO().updateRecipe(item)
            }
        }.start()
    }

    /**
     * Fonction qui gère la selection des Chips
     * */
    private fun handleChipSelected() {
        //Selon le Chip selectionné on filtre les données
        when {
            binding.chipRandom.isChecked -> {
                filterData("default")
            }
            binding.chipDish.isChecked -> {
                filterData("dish")
            }
            binding.chipFood.isChecked -> {
                filterData("food")
            }
            else -> {
                filterData("")
            }
        }
    }
}