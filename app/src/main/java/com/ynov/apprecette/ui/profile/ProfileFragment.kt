package com.ynov.apprecette.ui.profile

import android.app.Activity.RESULT_OK
import android.content.*
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ynov.apprecette.R
import com.ynov.apprecette.adapter.FavoriteAdapter
import com.ynov.apprecette.database.recipe.RecipeDatabase
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*

class ProfileFragment : Fragment() {

    //Variables view binding
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    //Variable d'état du FAB d'édition
    private var fabFlag = false

    //Variables d'accès aux databases et préférences utilisateurs
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    private lateinit var favoriteDatabase: RecipeDatabase
    private lateinit var recipeDatabase: RecipeDatabase

    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 0

    override fun onResume() {
        super.onResume()
        GlobalScope.launch(Dispatchers.Default) {
            launch(Dispatchers.Main) {
                getData()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        sharedPreferences = requireActivity().getSharedPreferences("USER_PREFERENCES", Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()

        getEditTextValues()

        favoriteDatabase = RecipeDatabase.getFavoriteDatabase(requireActivity().applicationContext)
        recipeDatabase = RecipeDatabase.getRecipeDatabase(requireActivity().applicationContext)

        binding.fabEdit.setOnClickListener() {
            if (fabFlag) {
                binding.fabEdit.setImageResource(R.drawable.ic_edit)
                saveEditTextValues()
                changeEnabledState(false)
            } else {
                binding.fabEdit.setImageResource(R.drawable.ic_check)
                changeEnabledState(true)
            }
        }

        binding.profileImg.setOnClickListener() {
            val items = arrayOf("Camera", "Gallery")

            if (fabFlag) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("Use an image from")
                    .setItems(items) { _, which ->
                        if (which == 0) {
                            capturePhoto()
                        } else {
                            getPhotoFromGallery()
                        }
                    }
                    .show()
            }
        }

        return binding.root
    }

    /**
     * Récupère les données de la database des favoris pour le recycler view
     */
    private fun getData() {
        binding.favoriteRecyclerView.layoutManager = LinearLayoutManager(activity)

        favoriteDatabase.recipeDAO().getAll().observe(this) {
            binding.favoriteRecyclerView.adapter = FavoriteAdapter(it.toTypedArray(), {item -> removeItem(item)})
        }
    }

    /**
     * Supprime un élément de la databse des favoris et le met à jour dans la database des recettes s'il est présent
     */
    private fun removeItem(item: RecipeItem) {
        Thread {
            favoriteDatabase.recipeDAO().deleteOne(item)
            item.isFavorite = false
            if(recipeDatabase.recipeDAO().isRowIsExist(item.uid!!)) {
                recipeDatabase.recipeDAO().updateRecipe(item)
            }
        }.start()
    }

    /**
     * Change tous les états selon la valeur passée
     * @param state Etat
     */
    private fun changeEnabledState(state: Boolean) {
        binding.pseudoTxt.isEnabled = state
        binding.txtInputFavDish.isEnabled = state
        binding.txtInputFavFood.isEnabled = state
        fabFlag = state
    }

    /**
     * Sauvegarde les données dans les préférences
     */
    private fun saveEditTextValues() {
        editor.putString("pseudo", binding.pseudoTxt.text.toString())
        editor.putString("favDish", binding.txtInputFavDish.text.toString())
        editor.putString("favFood", binding.txtInputFavFood.text.toString())
        editor.apply()
    }

    /**
     * Récupère les données dans les préférences
     */
    private fun getEditTextValues() {
        binding.pseudoTxt.setText(sharedPreferences.getString("pseudo", null))
        binding.txtInputFavDish.setText(sharedPreferences.getString("favDish", "Not specified"))
        binding.txtInputFavFood.setText(sharedPreferences.getString("favFood", "Not specified"))
        val imageUri = sharedPreferences.getString("imageUri", "")
        if(imageUri != "") {
            binding.profileImg.setImageURI(Uri.parse(imageUri))
        }
    }

    /**
     * Récupere une photo de la galerie
     */
    private fun getPhotoFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, REQUEST_IMAGE_GALLERY)
    }

    /**
     * Prend une photo
     */
    private fun capturePhoto() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {}
    }

    /**
     * Fonction appelée apres action de galerie ou camera
     * */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            var imageBitmap = data?.extras?.get("data") as Bitmap
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 300, 300, false)
            binding.profileImg.setImageBitmap(imageBitmap)
            editor.putString("imageUri", bitmapToFile(imageBitmap).toString())
            editor.apply()
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            val imageUri = data?.data
            binding.profileImg.setImageURI(imageUri)
            editor.putString("imageUri", imageUri.toString())
            editor.apply()
        }
    }

    // Method to save an bitmap to a file
    private fun bitmapToFile(bitmap: Bitmap): Uri {
        // Get the context wrapper
        val wrapper = ContextWrapper(context)

        // Initialize a new file instance to save bitmap object
        var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
        file = File(file, "${UUID.randomUUID()}.jpg")

        try{
            // Compress the bitmap and save in jpg format
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        }catch (e: IOException){
            e.printStackTrace()
        }

        // Return the saved bitmap uri
        return Uri.parse(file.absolutePath)
    }
}