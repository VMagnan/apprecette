package com.ynov.apprecette.ui.recipe

import android.os.Bundle
import android.text.Html
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import com.ynov.apprecette.R
import com.ynov.apprecette.database.recipe.RecipeDatabase
import com.ynov.apprecette.databinding.FragmentRecipeBinding

class RecipeFragment : Fragment() {

    //Variables view binding
    private var _binding: FragmentRecipeBinding? = null
    private val binding get() = _binding!!

    //Variable d'accès à la database
    private lateinit var recipeDatabase: RecipeDatabase
    private lateinit var favoriteDatabase: RecipeDatabase

    //Variable d'état favori
    private var isFavorite = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentRecipeBinding.inflate(inflater, container, false)

        setHasOptionsMenu(true)

        //Crée l'accès à la database
        recipeDatabase = RecipeDatabase.getRecipeDatabase(requireActivity().applicationContext)
        favoriteDatabase = RecipeDatabase.getFavoriteDatabase(requireActivity().applicationContext)

        //Récupère la source de l'item
        val source = arguments?.getString("source")
        //Récupère l'id de l'item
        val itemId = arguments?.getInt("item_id")

        //selon la source on apelle la fonction en passant l'id de l'item et la database correspondante
        if (source == "random") {
            setBinding(recipeDatabase, itemId)
        } else if (source == "favorite") {
            setBinding(favoriteDatabase, itemId)
        }

        //Configuration d'un click sur l'image favori
        binding.favoriteImg.setOnClickListener {
            //Si la recette était dans les favoris
            if (isFavorite) {
                //On inverse l'état et on change l'image
                isFavorite = false
                binding.favoriteImg.setImageResource(R.drawable.ic_star_outline)

                //Lance un thread qui appelle une fonction qui va supprimer la recette des favoris
                Thread {
                    removeFromFavDb(itemId!!)
                }.start()
            }
            //Si la recette n'était pas dans les favoris
            else {
                //On inverse l'état et on change l'image
                isFavorite = true
                binding.favoriteImg.setImageResource(R.drawable.ic_star)

                //Lance un thread qui appelle une fonction qui va ajouter la recette dans les favoris
                Thread {
                    saveInFavDb(itemId!!)
                }.start()
            }
        }
        return binding.root
    }

    /**
     * Fonction qui crée le binding selon la database et l'id passés
     * @param database Database specifique
     * @param itemId Id de l'item à afficher
     * */
    private fun setBinding(database: RecipeDatabase, itemId: Int?) {
        //Si l'id n'est pas null
        if (itemId != null) {
            //On récupère la recette selon son id
            database.recipeDAO().getLiveDataById(itemId).observe(viewLifecycleOwner) {

                //Bind des valeurs aux composants graphiques correspondant
                binding.recipeName.text = it.name
                binding.recipeLikes.text = it.likesNumber.toString()
                binding.recipeReadyIn.text = it.readyInMinutes.toString()
                binding.recipeServings.text = it.servings.toString()

                //Ajout de la gestion de texte en format html
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    binding.recipeInstructions.text = Html.fromHtml(it.instructions, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    binding.recipeInstructions.text = it.instructions
                }

                //Ajout d'un scrolling sur la textview
                binding.recipeInstructions.movementMethod = ScrollingMovementMethod()

                //Récupère l'URL de l'image pour la chargé dans la view correspondante
                Picasso.get().load(it.imageUrl).fit().into(binding.recipeImg)

                //Défini la variable d'état favori
                isFavorite = it.isFavorite

                //Selon son état on affiche un icone rempli ou non
                if(it.isFavorite) {
                    binding.favoriteImg.setImageResource(R.drawable.ic_star)
                } else {
                    binding.favoriteImg.setImageResource(R.drawable.ic_star_outline)
                }
            }
        }
    }

    /**
     * Enleve un element de la db des favoris et met à jour l'item dans la db des recettes si il y est présent
     * @param id Id de la recette à supprimer
     */
    private fun removeFromFavDb(id: Int) {
        //Récupère la recette et modifie son champ favori
        val item = favoriteDatabase.recipeDAO().getById(id)
        item.isFavorite = false

        //Supprime la recette dans la db favori
        favoriteDatabase.recipeDAO().deleteOne(item)

        //Met à jour la recette dans la db des recettes
        if(recipeDatabase.recipeDAO().isRowIsExist(id)) {
            recipeDatabase.recipeDAO().updateRecipe(item)
        }
    }

    /**
     * Ajoute un element de la db des favoris et met à jour l'item dans la db des recettes
     * @param id Id de la recette à ajouter
     * */
    private fun saveInFavDb(id: Int) {
        //Récupère la recette et modifie son champ favori
        val item = recipeDatabase.recipeDAO().getById(id)
        item.isFavorite = true

        //Ajoute la recette dans la db favori
        favoriteDatabase.recipeDAO().insertOne(item)

        //Met à jour la recette dans la db des recettes
        recipeDatabase.recipeDAO().updateRecipe(item)
    }
}