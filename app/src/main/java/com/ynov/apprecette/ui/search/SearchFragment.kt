package com.ynov.apprecette.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ynov.apprecette.adapter.SearchRecipeAdapter
import com.ynov.apprecette.data.RecipesList
import com.ynov.apprecette.data.RecipesService
import com.ynov.apprecette.database.recipe.RecipeDatabase
import com.ynov.apprecette.database.recipe.RecipeItem
import com.ynov.apprecette.databinding.FragmentSearchBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SearchFragment : Fragment() {
    //Variables view binding
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!

    //Variables d'accès aux databases
    private lateinit var favoriteDatabase: RecipeDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        //Création du ViewBinding
        _binding = FragmentSearchBinding.inflate(inflater, container, false)

        //Création des accès aux databases
        favoriteDatabase = RecipeDatabase.getFavoriteDatabase(requireActivity().applicationContext)

        //Configure la fonction de recherche
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                submitSearch(query!!)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
        return binding.root
    }

    /**
     * Fonction qui crée la requete de recherche de recettes
     * @param query Champ de la recherche
     * */
    private fun submitSearch(query: String) {
        //Création du service retrofit
        val retrofit = Retrofit.Builder().baseUrl("https://api.spoonacular.com/recipes/").addConverterFactory(GsonConverterFactory.create()).build()
        val service = retrofit.create(RecipesService::class.java)

        //Création de la requete de recherche
        val call = service.searchRecipes(query)

        //Affiche la progressBar
        binding.indeterminateBar.visibility = View.VISIBLE

        //Appel de la requete
        call.enqueue(object : Callback<RecipesList> {
            override fun onResponse(call: Call<RecipesList>, response: Response<RecipesList>) {
                val recipesList = response.body()

                val items = ArrayList<RecipeItem>()

                for (recipe in recipesList.recipes) {
                    val item = RecipeItem(null, recipe.name, recipe.imageUrl, recipe.readyInMinutes, recipe.servings, recipe.likesNumber, recipe.instructions, recipe.healthScore, false, "")
                    items.add(item)
                }

                binding.searchProductList.layoutManager = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)
                binding.searchProductList.adapter = SearchRecipeAdapter(items.toTypedArray()) { item, isFavorite ->
                    handleFavClick(
                        item,
                        isFavorite
                    )
                }

                binding.indeterminateBar.visibility = View.GONE
            }

            override fun onFailure(call: Call<RecipesList>, t: Throwable) {
                binding.indeterminateBar.visibility = View.GONE
                error("ERROR")
            }
        })
    }

    private fun handleFavClick(item: RecipeItem, isFav: Boolean) {
        Thread {
            if (isFav) {
                favoriteDatabase.recipeDAO().insertOne(item)
            } else {
                favoriteDatabase.recipeDAO().deleteOne(item)
            }
        }.start()
    }

    fun isFavorite(id: Int) : Boolean {
        return favoriteDatabase.recipeDAO().isRowIsExist(id)
    }
}